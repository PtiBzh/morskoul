#!/bin/bash

# Setup the database
# https://osm2pgsql.org/doc/manual.html#preparing-the-database
sudo -u postgres psql --command='DROP DATABASE IF EXISTS osm;'
sudo -u postgres psql --command='DROP USER IF EXISTS osmuser;'
sudo -u postgres createuser osmuser
sudo -u postgres createdb --encoding=UTF8 --owner=osmuser osm
sudo -u postgres psql osm --command='CREATE EXTENSION postgis;'
sudo -u postgres psql osm --command='CREATE EXTENSION hstore;'
sudo -u postgres psql osm --command="ALTER USER osmuser WITH PASSWORD 'newe';"

# Import data
osm2pgsql data/bretagne-latest.osm.pbf -d postgresql://osmuser:newe@localhost:5437/osm --output=flex --style=osm2pgsql_config.lua
