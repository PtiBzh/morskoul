# Requirements
* osm2postgres | 1.8.0 | https://github.com/openstreetmap/osm2pgsql
* osmium | 1.15.0 2.18.0 | https://github.com/osmcode/osmium-tool
* martin | 0.8.7 | https://github.com/maplibre/martin
* gdal | 3.6.2 | https://gdal.org/
