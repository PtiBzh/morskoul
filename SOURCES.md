# OSM
* http://download.openstreetmap.fr/extracts/europe/france/bretagne/

# SHOM
* https://diffusion.shom.fr/donnees/base-de-donnees-maritimes-et-littorales/balisage_maritime.html
* https://diffusion.shom.fr/donnees/base-de-donnees-maritimes-et-littorales/epaves.html

